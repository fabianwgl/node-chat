# NODE CHAT

Sample chat using Nodejs

- [Requirements](#requirements)
- [Installation](#installation)
  - [Setting up .env](#setting-up-env)


## Requirements

- [Node.js v10.x LTS](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/)
- [RabbitMQ](https://www.rabbitmq.com/)

## Installation

- Install dependencies: `npm install` / `yarn`
- Configure your own .env file (more info [here](#setting-up-env))
- Start the server: `npm run dev` / `yarn dev` for development with [nodemon](https://nodemon.io/), or `node index.js`

## Setting up .env

Here is an explanation of each key/value pair that must be defined at your .env file:

**NOTE**: _.env doesn't require (and it's not expecting) quotes for any of the defined values!_

| Key                   | Description                                                                                                                                             |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| MONGO_URI=      | Defines the MongoDB service.               |
| RABBIT_URI=      | Defines the RabbitMQ service |
| SECRET= |  Defines the encription secret key                    |
| PORT=          | Defines the port for the api to work                                                 |


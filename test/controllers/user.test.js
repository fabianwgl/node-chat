/* eslint no-unused-expressions: "off", no-underscore-dangle: "off" */
const chai = require('chai');

const UserController = require('../../src/controllers/user');
const userModel = require('../../src/db/models/user');

const expect = chai.expect;


describe('UserController', () => {
  it('Should exist and be an object', () => {
    expect(UserController).to.exist;
    expect(UserController).to.be.an('object');
  });

  describe('signin', () => {
    it('Should create a user', async() => {
      const data = {
        username: 'sample_username',
        password: 'sample_password'
      }

      const response = await UserController.signin(data);

      expect(response).to.have.a.property('_id')
      expect(response).to.have.a.property('username')
      expect(response).to.have.a.property('password')
    })
  })
});

const amqp = require('amqplib')

class RabbitMQ {
  constructor() {
    this.amqp = amqp;
    this.rabbitmq;
    this.initializeConnection();
  }

  async initializeConnection() {
    this.rabbitmq = await amqp.connect(process.env.RABBIT_URI);
  }

  async sendMessage(channel, message) {
    const createdChannel = await this.rabbitmq.createChannel();
    await createdChannel.assertQueue(channel)
    const msg = await createdChannel.sendToQueue(channel, Buffer.from(message))

    return msg;
  }
}

const rabbitmq = new RabbitMQ();
module.exports = rabbitmq;
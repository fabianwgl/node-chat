const { Schema } = require('mongoose');

const chatSchema = new Schema({
  sender: String,
  channel: String,
  message: String,
},{
  timestamps: true
})

module.exports = chatSchema;
const { model } = require('mongoose');

const chatSchema = require('./schema');

const chatModel = model('Chat', chatSchema);

module.exports = chatModel;

const { model } = require('mongoose');

const userSchema = require('./schema');

const userModel = model('User', userSchema);

module.exports = userModel;

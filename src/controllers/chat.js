const jwt = require('jsonwebtoken');
const userModel = require('../db/models/user');
const chatModel = require('../db/models/chat');
const rabbit = require('../db/rabbitmq')


const postMessage = async (sender, channel, message) => {
  if (!message || message === ' ') {
    throw new Error('please add a message')
  }
  
  const { username } = await jwt.decode(sender, process.env.SECRET);
  
  const foundUser = await userModel.findOne({username: username })

  const messageObject = {
    username,
    message
  }

  await rabbit.sendMessage(channel, JSON.stringify(messageObject))
  
  io.emit('chat message', JSON.stringify(messageObject));
  
  await chatModel.create({ sender: username, channel, message })

  return foundUser
}

module.exports = { postMessage };
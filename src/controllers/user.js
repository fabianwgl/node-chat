const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const userModel = require('../db/models/user')

const signin = async(body) => {
  const hashedPassword = await bcrypt.hash(body.password, 10);
  
  const createdUser = await userModel.create({
    username: body.username,
    password: hashedPassword
  });

  return createdUser
}

const login = async(body) => {
  const foundUser = await userModel.findOne({ username: body.username });

  const isPasswordOk = await bcrypt.compare(body.password, foundUser.password);

  if (!isPasswordOk) {
    throw new Error('password does not match')
  }

  const token = await jwt.sign(JSON.stringify(foundUser), process.env.SECRET);

  return token;
}

module.exports = { signin, login };
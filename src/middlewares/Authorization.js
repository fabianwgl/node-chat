const jwt = require('jsonwebtoken')
const asyncHandler = require('express-async-handler');

const validateToken = asyncHandler( async (req, res, next) => {
  const { authorization } = req.headers;
  const token = authorization.replace('Bearer ', '');
  
  const isTokenValid = await jwt.verify(token, process.env.SECRET);

  if(!isTokenValid) {
    throw new Error ('Token is not valid, please login again');
  }

  req.headers.authorization = token;

  next();
})

module.exports = { validateToken };
const path = require('path');
const router = require('express').Router();
const asyncHandler = require('express-async-handler');

const chatModel = require('../../db/models/chat')
const { validateToken } = require('../../middlewares/Authorization')
const { postMessage } = require('../../controllers/chat')

router.get('/private/chat', asyncHandler( async(req, res) => {
  res.sendFile(path.resolve('src/public/index.html'))
}))

router.get('/private/chat/:channel/last50', asyncHandler( async(req, res) => {
  //  Have the chat messages ordered by their timestamps and show only the last 50 messages.
  const chats = await chatModel.find({}).sort({'createdAt': 1}).limit(50); 
  res.send(chats)
}))

router.post('/private/chat/:channel/message', validateToken,  asyncHandler( async(req, res) => {
  const { channel } = req.params;
  const { authorization } = req.headers;
  const { message } = req.body 
  
  const created = await postMessage(authorization, channel, message);
  
  res.send(created);
}));

module.exports = router;
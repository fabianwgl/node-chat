const usersPublic = require('./public/users')
const chatPrivate = require('./private/chat')

module.exports = [usersPublic, chatPrivate]
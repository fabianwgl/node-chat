const path = require('path');
const router = require('express').Router();
const asyncHandler = require('express-async-handler');

const { signin, login } = require('../../controllers/user')


router.get('/', (req, res) => {
  res.sendFile(path.resolve('src/public/login.html'))
})

router.post('/signin', asyncHandler( async(req, res) => {
  const { username, password } = req.body;

  const created = await signin({username, password});

  res.send(created);
}));

router.post('/login', asyncHandler( async(req, res) => {
  const { username, password } = req.body;
  
  const created = await login({username, password});

  res.send(created);
}));

module.exports = router;
require('dotenv').config()
const http = require('http'); // Required by Socket.IO
const express = require('express');
const SocketIO = require('socket.io');

const cors = require('cors');

require('./src/db')
require('./src/db/rabbitmq')

const mainRouter = require('./src/routes')

// Create Express app
const app = express();

// Disable X-Powered-By header
app.disable('x-powered-by');

app.use(cors());

app.use(express.json());
app.use(express.static('src/public/assets'));
app.use(mainRouter);

const server = http.createServer(app);

const io = new SocketIO(server);

global.io = io;

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });
});

server.listen(process.env.PORT, () => console.log(`Initialized server at port ${process.env.PORT}`));